#!/bin/bash

if [ $# -lt 1 ]; then
  echo "Usage: $0 configuration"
  exit 1
fi

# read configuration
config=$1
if [ -f $1 ]; then
  source $config
else
  echo "$config: File not found!"
  exit 1
fi

# prepare gluonbuild podman image
podman image exists ${buildimagetag}
if [ $? -ne 0 ]; then
  echo -en ">>> Create podman build container\n"
  podman build -t ${buildimagetag} -f ${dockerfile}
  echo -en ">>> done\n"
else
  echo -en ">>> Build container exists\n"
fi

# prepare sources
podrun="podman run -it --rm -v $(pwd)/${builddir}:/gluon ${buildimagetag}"
## check if builddir exists
if [ ! -d ${builddir} ]; then
  ## builddir does not exist -> clone gluon and site config

  ### gluon
  echo -en ">>> Builddir doesn't exist... cloning gluon@${gluonref} from ${gluongit}\n"
  git clone ${gluongit} ${builddir}
  pushd ${builddir} > /dev/null
  git checkout -b ${sitebranch} ${gluonref}

  ### site.conf
  echo -en ">>> Cloning site configuration ${sitebranch} from ${sitegit}\n"
  git clone ${sitegit} -b ${sitebranch} site
  popd >> /dev/null

  ### check builddir accessibility for container
  echo -en ">>> Checking access permissions on source files..."
  if [ "$(stat -c %u ${builddir})" == "$(id -u)" ]; then
    echo -en " changing..."
    podman unshare chown -R 1000:1000 ${builddir}
  else
    echo -en " ok..."
  fi
  echo -en " done\n"
else
  ## builddir exists check if gluon and site.conf have the right versions

  ### gluon
  echo -en ">>> Builddir exists\n"
  chkremote="$(${podrun} bash -c 'git remote -v' | grep ${gluongit})"
  if [ -z "${chkremote}" ]; then
    echo -en ">>> Configured git remote ${gluongit} doesn't match the gluon remote in ${builddir}. Please check and remove the old builddir if possible.\n"
    exit 1
  fi
  echo -en ">>> Checking out gluon@${gluonref}\n"
  ${podrun} bash -c "git checkout ${gluonref}"

  ### site.conf branch
  if [ -d ${builddir}/site ]; then
    echo -en ">>> Site configuration exists\n"
    chkremote="$(${podrun} bash -c 'cd site && git remote -v' | grep ${sitegit})"
    if [ -z "${chkremote}" ]; then
      echo -en ">>> Git remote changed. Resetting site configuration to ${sitebranch} from ${sitegit}\n"
      ${podrun} rm -rf site
      ${podrun} git clone ${sitegit} -b ${sitebranch} site
    else
      echo -en ">>> Updating site configuration\n"
      ${podrun} bash -c "cd site && git fetch origin"
      ${podrun} bash -c "cd site && git checkout -B ${sitebranch} origin/${sitebranch}"
    fi
  else
    echo -en ">>> Cloning site configuration: branch ${sitebranch} from ${sitegit}\n"
    ${podrun} git clone -b ${sitebranch} ${sitegit} site
  fi
fi

# if siteref is set and non-empty -> checkout that exact git ref
if [[ -n ${siteref} ]]; then
  echo -en ">>> Checking out site configuration ${sitebranch}${siteref:+ @ ${siteref}}\n"
  ${podrun} bash -c "cd site && git checkout ${siteref}"
fi

# run build
echo -en "\n>>> Starting build run...\n"
podman run -it --rm \
  -v $(pwd)/${builddir}:/gluon \
  ${GLUON_VERSION:+-e "GLUON_VERSION=$GLUON_VERSION"} \
  ${GLUON_BRANCH:+-e "GLUON_BRANCH=$GLUON_BRANCH"} \
  ${GLUON_TAG:+-e "GLUON_TAG=$GLUON_TAG"} \
  ${GLUON_RELEASE:+-e "GLUON_RELEASE=$GLUON_RELEASE"} \
  ${buildimagetag} ./site/build.sh

# vim: ft=sh ts=2 sw=2 :
