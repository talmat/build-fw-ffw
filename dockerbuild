#!/bin/bash

if [ $# -lt 1 ]; then
  echo "Usage: $0 configuration"
  exit 1
fi

# read configuration
config=$1
if [ -f $1 ]; then
  source $config
else
  echo "$config: File not found!"
  exit 1
fi

# prepare gluonbuild image
docker image inspect ${buildimagetag} > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo -en ">>> Create build container...\n"
docker build -t ${buildimagetag} -f ${dockerfile} .
  echo -en ">>> done.\n"
else
  echo ">>> Build container exists... continue."
fi

# prepare sources
## check if builddir exists
if [ ! -d ${builddir} ]; then
  ## builddir does not exist -> clone gluon and site config

  ### gluon
  echo -en ">>> Builddir doesn't exist... cloning gluon@${gluonref} from ${gluongit}\n"
  git clone ${gluongit} ${builddir}
  pushd ${builddir} > /dev/null
  git checkout -b ${sitebranch} ${gluonref}

  ### site.conf
  echo -en ">>> Cloning site configuration ${sitebranch} from ${sitegit}\n"
  git clone ${sitegit} -b ${sitebranch} site
  popd > /dev/null
else
  ## builddir exists check if gluon and site.conf have the right versions

  ### gluon
  echo -en ">>> Builddir exists\n"
  pushd ${builddir} > /dev/null
  chkremote="$(git remote -v | grep ${gluongit})"
  if [ -z "${chkremote}" ]; then
    echo -en ">>> Configured git remote ${gluongit} doesn't match the gluon remote in ${builddir}. Please check and remove the old builddir if possible.\n"
    exit 1
  fi
  echo -en ">>> Checking out gluon@${gluonref}\n"
  git checkout ${gluonref}
  popd > /dev/null

  ### site.conf branch
  pushd ${builddir} > /dev/null
  if [ -d site ]; then
    echo -en ">>> Site configuration exists\n"
    pushd site > /dev/null
    chkremote="$(git remote -v | grep ${sitegit})"
    popd > /dev/null
    if [ -z "${chkremote}" ]; then
      echo -en ">>> Git remote changed. Resetting site configuration to ${sitebranch} from ${sitegit}\n"
      rm -rf site
      git clone ${sitegit} -b ${sitebranch} site
    else
      echo -en ">>> Updating site configuration\n"
      pushd site > /dev/null
      git fetch origin
      git checkout -B ${sitebranch} origin/${sitebranch}
      popd > /dev/null
    fi
  else
    echo -en ">>> Cloning site configuration: branch ${sitebranch} from ${sitegit}\n"
    git clone -b ${sitebranch} ${sitegit} site
  fi
  popd > /dev/null
fi


  # if siteref is set and non-empty -> checkout that exact git ref
if [[ -n ${siteref} ]]; then
  echo -en ">>> Checking out site configuration ${sitebranch}${siteref:+ @ ${siteref}}\n"
  pushd ${builddir}/site > /dev/null
  git checkout ${siteref}
  popd > /dev/null
fi

# run build
echo ">>> Starting build run..."
docker run -it --rm \
  --user=$(id -u):$(id -g) \
  -v $(pwd)/${builddir}:/gluon \
  ${GLUON_VERSION:+-e "GLUON_VERSION=$GLUON_VERSION"} \
  ${GLUON_BRANCH:+-e "GLUON_BRANCH=$GLUON_BRANCH"} \
  ${GLUON_TAG:+-e "GLUON_TAG=$GLUON_TAG"} \
  ${GLUON_RELEASE:+-e "GLUON_RELEASE=$GLUON_RELEASE"} \
  ${buildimagetag} ./site/build.sh

# vim: ft=sh ts=2 sw=2 :
