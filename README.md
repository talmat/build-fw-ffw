# Freifunk Wuppertal firmware build automation

## Purpose
This repo holds the top level build automation for the ffw firmware releases. Its purpose is to

  - simplify and automate the build process
  - make it "reproducible"
  - serve as documentation and reference about how past releases have been built

## Prerequisites
The build host should have a working installation of docker or podman.

## Cloning
For the time beeing you can get this repo by cloning it via:

`git clone http://wupper8.ffwtal.net/git/build-fw-ffw.git`

## Usage
The repo contains configuration files (that correspond to firmware relases) and docker/podman flavours of a script
that serves to:

  - set up the build directory and download all necessary sources 
  - create a docker/podman container for the needed build toolchain
  - execute the version dependent build script from the site-ffw repo inside the container

both scripts should be called in the repos main directory.

### podman or docker
Since the author of this repo somewhat dislikes the idea of the central docker daemon and prefers to have a
rootless container for the build toolchain there are two versions of the build script. Namely dockerbuild and podmanbuild.

Both are equivalent in function and are both called with one of the configuration files as single parameter. Which one you
choose is simply a matter of taste and what's available on your build host. It shouldn't influence the generated images.

Since the build directory is created as a simple subfolder of the repos main directory there needs some care to be taken
when you're using the rootless method via podman. Since the container needs to be able to have read/write access to the
build directory it needs to be owned by a special uid/gid mandated by the linux subuid/subgid mechanism - used by podman -
to achieve the rootless feature.

When the builddirectory is created from scratch by the podmanbuild script this will automatically be taken care of.
If you need to change the permissions, either from your local users uid/gid to the subuid/subgid needed by the container,
or from the containers user to you local system user account, there are two more scripts included to do this.

`podman_chown2builduser` and `podman_chown2sysuser`

both need to be called with the build configuration file as a parameter and will change the corresponding build directories
permissions as their names suggest. If you want to known whats going on you can read up a nice blog post from the podman
developers that can be found here: [Running rootless Podman as a non-root user](https://www.redhat.com/sysadmin/rootless-podman-makes-sense)

### Example build run
    git clone http://wupper8.ffwtal.net/git/build-fw-ffw.git
    cd build-fw-ffw
    ./dockerbuild ffw-release.conf or ./podmanbuild ffw-release.conf

Each of these commands creates the build directory in the current path constructs the toolchain container and runs the build.
The name of the build directory and the created container is pre set in the chosen configuration file. Be aware of the fact
that the build directory will take up a ***considerable*** amount of space when the build proceedes. For example - at the
time of writing - the build folder for a full gluon v2020.2 build takes up around 80GB of diskspace when the build finishes.

After the build has finished the generated gluon images and packages can be found in the build directories "output" subfolder.

## Creating new configuration files
Take a look at the included example configuration file "ffw-example.config". The file is pretty short and there are two types
of variables that can be set.

The lowercase ones in the beginning will be used by the docker-/podmanbuild script itself to clone the right git repos and
the appropriate branches and git commit references.

The uppercase "GLUON_" ones will be passed to the build script that accompanies the site configuration. If they are not set
the defaults from the build script will be used. They can be set however to override those defaults. This can be used for
example to produce a build for the stable branch of the firmware, while the build script in the site.conf repo defaults to
building an image for the experimental branch. You can also set the priority value for the gluon autoupdater package from here.

### Variables
  - docker-/podmanbuild specific:
    - **builddir**: name of the created build directory
    - **gluongit**: the gluon repository URL (https://github.com/freifunk-gluon/gluon)
    - **gluonref**: the tag of the gluon release or a specific git commit hash
    - **sitegit**: the site.conf git repo URL
    - **sitebranch**: the site repos branch to check out (ffw site.conf repo is organized in branches)
    - **siteref**: git tag or hash of a specific commit in the site repo (used to pin stable release versions)
  - gluon/site specific:
    - note: by default the release name gets created by the following scheme: `wup-$(date)-$GLUON_TAG-$GLUON_VERSION`
      the GLUON_RELEASE variable can override this and will completely replace the above scheme when set. When using it
      setting the variables GLUON_VERSION, GLUON_TAG will have no effect.
    - **GLUON_VERSION**: can be set to the gluon version the build is using
    - **GLUON_TAG**: this sets the short tag of the branch to be built. i.e. sta, exp, alpha, beta etc.
    - **GLUON_RELEASE**: as explained above. Replaces the default naming scheme with whtever you like
    - **GLUON_BRANCH**: this is used in the manifest creation step of the build to set the autoupdater branch
    - **GLUON_PRIORITY**: this defines the gluon autoupdaters priority value set in the manifest
